#!/bin/sh
#
# usage: retrieve-cert.sh remote.host.name [port] [additional-args]
#
REMHOST=$1
REMPORT=${2:-443}

shift 2

echo |\
openssl s_client -connect ${REMHOST}:${REMPORT} $@ 2>&1 |\
sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
