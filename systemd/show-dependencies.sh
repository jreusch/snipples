#! /bin/env sh

dir="$(mktemp -d)"

defaultunit="graphical.target"

allunits="$(systemctl --all | grep "^ " | awk '{print $1}' | tail -n +2 | sort)"

for unit in $allunits; do
	echo Processing "$unit"
	systemd-analyze dot -- "$unit" 2> /dev/null | dot -Tsvg |  sed -E -e "s/<text(.*)>(.+)<\/text>/<a xlink:href=\"\2.svg\">\n<text\1>\2<\/text>\n<\/a>/" > "$dir/$unit.svg"
done
echo Done

firefox "$dir/$defaultunit.svg"
