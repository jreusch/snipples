#!/usr/bin/env bash
set -e

DOMAINS=${@}
MAIL="ssl@sysmail.rz-42.de"

DIR="$(readlink -f $(dirname ${0}))"
CONFIGDIR="${DIR}/config"

HOSTSFILE="${CONFIGDIR}/hosts.conf"
OPENSSLTEMPLATE="${CONFIGDIR}/openssl.cnf"

DATE="$(date +%Y%m%d%H%M%S)"


function readconfig() {
    file=$1
    shift
    sed -n "s/${@}\s*=\s*\(.*\)/\1/p" ${file}
}

DAYS=1095

umask "077"

for DOMAIN in ${DOMAINS}; do
    TMPDIR="$(mktemp -d)"

    CN=$(echo ${DOMAIN})

    ALTNAME=""
    i=0
    for HOST in $(grep "^${DOMAIN}" ${HOSTSFILE}); do
        let ++i
        ALTNAME="${ALTNAME}DNS.${i} = ${HOST}\n"
    done

    OPENSSLCNF="${TMPDIR}/openssl.cnf"

    sed "s/###commonName_default###/commonName_default = ${CN}/;s/###alt_names###/${ALTNAME}/" "${OPENSSLTEMPLATE}" > "${OPENSSLCNF}"
#    openssl req -newkey rsa:2048 -keyout "${TMPDIR}/key.key" -out "${TMPDIR}/request.csr" -nodes -config "${OPENSSLCNF}"
#    openssl genrsa -out "${TMPDIR}/key.pem" 2048 -config "${OPENSSLCNF}"
#    openssl req -new -key "${TMPDIR}/key.pem" -out "${TMPDIR}/request.csr" -config "${OPENSSLCNF}" -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

    #irgendiw funktioniert das subject generieren aus der config file nicht.
    #darum gibt es diesen haesslichen quirk
    country=$(readconfig ${OPENSSLCNF} countryName_default)
    state=$(readconfig ${OPENSSLCNF} stateOrProvinceName_default)
    locality=$(readconfig ${OPENSSLCNF} localityName_default)
    organization=$(readconfig ${OPENSSLCNF} 0.organizationName_default)
    organizationalunit=$(readconfig ${OPENSSLCNF} organizationalUnitName_default)
    commonname=$(readconfig ${OPENSSLCNF} commonName_default)
    email=$(readconfig ${OPENSSLCNF} emailAddress_default)

    openssl req -newkey rsa:4096 -days ${DAYS} -keyout "${TMPDIR}/${DATE}.key" -out "${TMPDIR}/${DATE}.csr" -nodes -config "${OPENSSLCNF}" -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

    mkdir -p ${DIR}/${CN}
    cp ${TMPDIR}/${DATE}.* ${DIR}/${CN}/

    rm -rf ${TMPDIR}
done

#damit kann man den inhalt pruefen
#openssl req -text -noout -in request.csr

