#!/bin/sh

[ -z "$CREATE_ZIP" ] && $CREATE_ZIP=false
[ -z "$ALBUM_NAME" ] && $ALBUM_NAME="Home"

node node_modules/.bin/thumbsup --theme cards \
	--input images --output images/out --photo-download copy \
	--no-usage-stats --usage-stats false --cleanup true \
	--album-zip-files $CREATE_ZIP \
	--home-album-name "$ALBUM_NAME"

	
