#!/bin/sh

backup() {

	dev="/dev/mapper/$1"
	cryptsetup isLuks $dev
	if [ $? -ne 0 ]; then
		echo "$dev is no luks device!"
		exit 1
	fi

	cryptsetup luksHeaderBackup $dev --header-backup-file ${1}.cryptheader
}


for i in "$@"; do
	backup $i
done
