cat > .git/hooks/post-update <<EOF
git update-server-info
unset GIT_DIR GIT_WORK_TREE
cd ../ && git reset --hard
EOF

chmod +x .git/hooks/post-update
git config receive.denyCurrentBranch ignore
