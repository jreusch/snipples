#!/bin/sh -ex

# Run docker container where the current directory is mounted read only but still writeable inside the container
# pass additional paths relative(!) to the current pwd to be writeable as $PERSISTENT_WRITEABLE space separated list.

TMP=$(mktemp -d)

UPPER=${TMP}/upper
WORK=${TMP}/work

CONTAINER_TARGET=/data

mkdir $UPPER $WORK

WRITEABLE=
for i in $PERSISTENT_WRITEABLE; do
     WRITEABLE="$WRITEABLE --mount type=bind,source=${PWD}/${i},target=${CONTAINER_TARGET}/${i}"
done


docker run -it --rm \
  --mount type=volume,dst=${CONTAINER_TARGET},volume-driver=local,volume-opt=type=overlay,\"volume-opt=o=lowerdir=$(pwd),upperdir=${UPPER},workdir=${WORK}\",volume-opt=device=overlay \
  $WRITEABLE \
  -w ${CONTAINER_TARGET} \
  debian:buster /bin/bash

