#!/bin/sh -e

# run all files in /etc/my_init if existing and continue with CMD if sucessfull
if [ -d "/etc/my_init" ]; then
    run-parts --exit-on-error /etc/my_init
fi
exec "$@"
