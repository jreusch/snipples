#!/bin/sh

cat <<EOF
This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache
EOF
docker system prune -a -f 

