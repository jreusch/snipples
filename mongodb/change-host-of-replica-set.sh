#!/bin/bash -e

host=$1
[ -z "$host" ] && host=$(hostname -s)

CMD="
cfg = rs.conf()
cfg.members[0].host = \"${host}:27017\"
rs.reconfig(cfg)
"

mongo --eval "${CMD}"
