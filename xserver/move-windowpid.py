#!/usr/bin/env python3
# moves a window to the center of the screen of the provided connection
# call with <script> window_pid screen_connection [width] [height]
import subprocess
import sys

def usage():
    print("usage: <script> window_pid screen_connection [width] [height]")

if (len(sys.argv) < 3):
    usage()
    sys.exit(1)

pid = sys.argv[1]
scr = sys.argv[2]

height = 300
width = 300
if (len(sys.argv) >= 4):
    width = int(sys.argv[3])
    height = int(sys.argv[4])

# just a helper function, to reduce the amount of code
get = lambda cmd: subprocess.check_output(cmd).decode("utf-8")

# get the data on all currently connected screens, as tuple (connectionm, [width,height,offset_x,offset_y])
screendata = [l.split() for l in get(["xrandr"]).splitlines() if " connected" in l]
screendata = sum([[(w[0], list(map(int, s.split("+")[0].split("x") + s.split("+")[-2:]))) for s in w if s.count("+") == 2] for w in screendata], [])

# get window_id for pid
get_windowid = lambda pid: [line.split()[0] for line in get(["wmctrl", "-l", "-p"]).splitlines() if line.split()[2] == pid][0]

#print("got screendata", screendata)

try:
    window_id = get_windowid(pid)
except IndexError:
    print(pid, "no window found for pid")
else:
    try:
        # determine the left position of the targeted screen (x)
        pos = [sc for sc in screendata if sc[0] == scr][0]
    except IndexError:
        # warning if the screen's name is incorrect (does not exist)
        print(scr, "does not exist. Check the screen name")
    else:
        #offset_y + screen_height/2 - height/2
        top = pos[1][3] + pos[1][1]//2 - height//2
        #offset_x + screen_width/2 - width/2
        left = pos[1][2] + pos[1][0]//2 - width//2
        subprocess.Popen(["wmctrl", "-ir", window_id, "-e", "0,{},{},{},{}".format(left,top,width,height)])
