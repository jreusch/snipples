#!/usr/bin/env python3
# moves a window to the center of the screen of the provided connection
# call with <script> window_pid screen_connection [width] [height]
import subprocess
import sys

def usage():
    print("usage: <script> screen")

if (len(sys.argv) <= 1):
    usage()
    sys.exit(1)

scr = sys.argv[1]

# just a helper function, to reduce the amount of code
get = lambda cmd: subprocess.check_output(cmd).decode("utf-8")

# get the data on all currently connected screens, as tuple (connectionm, [width,height,offset_x,offset_y])
screendata = [l.split() for l in get(["xrandr"]).splitlines() if " connected" in l]
screendata = sum([[(w[0], list(map(int, s.split("+")[0].split("x") + s.split("+")[-2:]))) for s in w if s.count("+") == 2] for w in screendata], [])

try:
    pos = [sc for sc in screendata if sc[0] == scr][0]
except IndexError:
    # warning if the screen's name is incorrect (does not exist)
    print(scr, "does not exist. Check the screen name")
    sys.exit(1)


print("width={}".format(pos[1][0]))
print("height={}".format(pos[1][1]))
print("offset_x={}".format(pos[1][2]))
print("offset_y={}".format(pos[1][3]))
