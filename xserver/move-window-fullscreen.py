#!/usr/bin/env python3
# moves a window to the to the screen connected to the given port and makes it fullscreen
# call with <script> window_id screen_connection
import subprocess
import sys

def usage():
    print("usage: <script> window_id screen_connection")

if (len(sys.argv) < 2):
    usage()
    sys.exit(1)

wid = sys.argv[1]
scr = sys.argv[2]

# just a helper function, to reduce the amount of code
get = lambda cmd: subprocess.check_output(cmd).decode("utf-8")

# get the data on all currently connected screens, as tuple (connectionm, [width,height,offset_x,offset_y])
screendata = [l.split() for l in get(["xrandr"]).splitlines() if " connected" in l]
screendata = sum([[(w[0], list(map(int, s.split("+")[0].split("x") + s.split("+")[-2:]))) for s in w if s.count("+") == 2] for w in screendata], [])

# get window_id for pid
get_windowid = lambda pid: [line.split()[0] for line in get(["wmctrl", "-l", "-p"]).splitlines() if line.split()[2] == pid][0]

try:
    # determine the left position of the targeted screen (x)
    pos = [sc for sc in screendata if sc[0] == scr][0]
except IndexError:
    # warning if the screen's name is incorrect (does not exist)
    print(scr, "does not exist. Check the screen name")
else:
    # first move and resize the window, to make sure it fits completely inside the targeted screen
    # else the next command will fail...
    # wmctrl -e 'g,x,y,w,h'
    subprocess.Popen(["wmctrl", "-ir", wid, "-e", "0,{x},{y},{w},{h}".format(x=int(pos[1][2])+100, y=int(pos[1][3])+100, w=300, h=300)])
    # maximize the window on its new screen
    #subprocess.Popen(["xdotool", "windowsize", "-sync", wid, "100%", "100%"])
    subprocess.Popen(["wmctrl", "-ir", wid, "-b", "add,maximized_vert,maximized_horz"])
