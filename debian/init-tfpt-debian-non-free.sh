#!/bin/bash
##########################################################################
# 
# Use this script to :
#  - download the pxelinux.0 (A), kernel (B) and initrd (C) for debian wheezy
#  - add all the non free firmware to the initrd 
#  - update your tftp directory
#
# Example of a tftp directory :
#  /srv/tftp/
#  |-- boot
#  |   |-- debian-7.0-amd64-initrd.gz (C)
#  |   `-- debian-7.0-amd64-linux (B) 
#  |-- pxelinux.0 (A)
#  `-- pxelinux.cfg
#
# Usage :
#  ./init-tfpt-debian-non-free.sh
#
# Required packages :
#  - wget
#  - pax
# 
# See : 
#  - http://wiki.debian.org/DebianInstaller/NetbootFirmware
#  - https://gist.github.com/1815282
#
##########################################################################

# tftp directory
TFTP="/srv/tftp"

# temporary working directory
TMP_TFTP="/tmp/tftp"

# debian netboot url
URL="http://ftp.fr.debian.org/debian/dists/wheezy/main/installer-amd64/current/images/netboot/debian-installer/amd64"

# initrd name (same as <%= @initrd %> in your foreman pxe template)
INITRD="debian-7.0-amd64-initrd.gz"

# linux kernel (same as <%= @kernel %> in your foreman pxe template)
KERNEL="debian-7.0-amd64-linux"

# Install pax if needed
dpkg -l | grep pax || aptitude install pax

# Add the non-free firmware to the initrd
rm    -rf $TMP_TFTP
mkdir -p  $TMP_TFTP/firmware
mkdir -p $TMP_TFTP/pxe

cd    $TMP_TFTP
wget "http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/wheezy/current/firmware.tar.gz"
tar -C firmware -zxf firmware.tar.gz
pax -x sv4cpio -s'%firmware%/firmware%' -w firmware | gzip -c >firmware.cpio.gz

cd    $TMP_TFTP/pxe
wget -q -O debian-initrd.gz $URL/initrd.gz
cp debian-initrd.gz{,.orig}
cat debian-initrd.gz.orig $TMP_TFTP/firmware.cpio.gz > $INITRD

# Update the initrd
cp $INITRD $TFTP/boot/$INITRD

# Update the pxe
wget -q $URL/pxelinux.0
cp   pxelinux.0 $TFTP/pxelinux.0

# Update the kernel
wget -q -O debian-linux $URL/linux
cp debian-linux  $TFTP/boot/$KERNEL

# Don't touch the boot files (because foreman-proxy can update these files)
chmod ugo-w $TFTP/boot/$KERNEL
chmod ugo-w $TFTP/boot/$INITRD

