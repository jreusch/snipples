#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader

PORT_START=7790

ME = {'name': '<hostname1>', 'dev_prefix': '/dev/zvol/rpool/VMS/', 'ip': '<ip1>'}
OTHER = {'name': '<hostname2>', 'dev_prefix': '/dev/mapper/', 'meta_prefix': '/dev/vg0/', 'ip': '<ip2>'}

resources = [
    {'name': 'jan-neu', 'otherdev': 'vm-jan-neu', 'index': 2}, 
]

file_loader = FileSystemLoader(".")
env = Environment(loader=file_loader)


for res in resources:
	port = PORT_START + res['index']
	template = env.get_template('resources.template')
	output = template.render(me=ME, other=OTHER, res=res, port=port)
	fname = "{}.res".format(res['name'])
	with open(fname, "w") as f:
		f.write(output)
	print(fname + " finished.")


