# python3 -m venv venv
# source venv/bin/activate
# pip install selenium

from selenium import webdriver
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.
driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver', options=chrome_options,
  service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])
driver.get('https://python.org')
print(driver.title)
