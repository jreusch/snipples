FROM debian:buster

RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends chromium-driver python3-pip
WORKDIR /app
RUN pip3 install selenium
COPY hello-world.py .

CMD python3 hello-world.py
