#!/usr/bin/python3

import ntplib
import sys
from time import ctime

if len(sys.argv) < 2:
    print("please provide NTP server on command line")
    sys.exit(1)
c = ntplib.NTPClient()
response = c.request(sys.argv[1])
print(ctime(response.tx_time))
