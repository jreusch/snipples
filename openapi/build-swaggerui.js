#!/usr/bin/env node

const commander = require('commander');

/**
 * scripts/build-swaggerui.js
 *
 * This script builds Swagger UI under static/ and generates a swagger.json file containing the openapi spec. It can
 * be triggered either by directly calling the script, or as a serverless post-deploy hook with serverless-stack-output
 */

const path = require('path');
const fs = require('fs');
const { getAbsoluteFSPath } = require('swagger-ui-dist');
//const { validate } = require('openapi-schema-validation');

const CONFIG = `
window.onload = function() {
    //<editor-fold desc="Changeable Configuration Block">

    // the following lines will be replaced by docker/configurator, when it runs in a docker-container
    window.ui = SwaggerUIBundle({
        url: "#URL#",
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
            SwaggerUIBundle.presets.apis,
            SwaggerUIStandalonePreset
        ],
        plugins: [
            SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
    });

    //</editor-fold>
};
`;

function parseOptions() {
    const options = commander
        .name('Build SwaggerUI')
        .description('Bundle openapi spec standalone with static swaggerUI')
        .version('0.0.1')
        .arguments('<input_spec>', 'Path to input openapi spec file')
        .arguments('<output_dir>', 'Path to write swagger-ui to ')
        .parse(process.argv);
    return options;
}
function deploySwaggerUI(specPath, outputPath) {
    const apispec = JSON.parse(fs.readFileSync(specPath));
    // validate openapi and warn in case there are issues
    // const { valid, errors } = validate(apispec, 3);
    // if (valid) {
    //     console.info('[success] Document is valid OpenAPI v3!');
    // } else {
    //     console.warn(
    //         '[warning] Document is not valid OpenAPI!',
    //         `${errors.length} validation errors:`,
    //         JSON.stringify(errors, null, 2),
    //     );
    // }

    // write to swagger.json file under static/
    const swaggerFile = path.join(outputPath, 'swagger.json');
    fs.writeFileSync(swaggerFile, JSON.stringify(apispec));
    console.info(`[info] Wrote OpenAPI spec to ${path.basename(outputPath)}/${path.basename(swaggerFile)}`);

    // copy swagger ui dist files
    const swaggerDist = getAbsoluteFSPath();
    const swaggerFiles = fs.readdirSync(swaggerDist).filter((file) => !file.endsWith('.map'));
    for (const file of swaggerFiles) {
        const source = path.join(swaggerDist, file);
        const target = path.join(outputPath, file);
        fs.writeFileSync(target, fs.readFileSync(source));
    }
    console.info(`[info] Copied ${swaggerFiles.length} SwaggerUI files to ${path.basename(outputPath)}/`);

    // replace api url to relative ./swagger.json in index.html
    const replaced = CONFIG.replace('#URL#', './swagger.json');
    fs.writeFileSync(path.join(outputPath, 'swagger-initializer.js'), replaced);
    console.info(`[info] Config successfully written.`);
}
function main() {
    const options = parseOptions();
    if (options.args.length != 2) {
        console.warn('Missing arguments!');
        options.help();
    }
    const specFile = options.args[0];
    const outputPath = options.args[1];
    fs.existsSync(specFile);
    fs.mkdirSync(outputPath, { recursive: true });
    deploySwaggerUI(specFile, outputPath);
}
main();
