#!/bin/sh

if [ ! -e "$1" ]; then
  echo "file does not exist"
  exit 1
fi

f=${1%.pdf}
pdfjam --outfile ${f}-a4.pdf --paper a4paper $1
