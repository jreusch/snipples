#!/usr/bin/python3
"""
    @author Jan Reusch <jan@jreusch.de>

    Benchmark XMLRPC Service
    HTTPs support only without certificate verificatio so far.
"""
import xmlrpc.client
#import requests_transport
import time
import argparse
import threading

#api = xmlrpc.client.ServerProxy('https://www.planet-lab.eu/PLCAPI/', transport=requests_transport.RequestsTransport())

parser = argparse.ArgumentParser(description='Test xmlrpc performance')
parser.add_argument('url', help='url to the xmlrpc service to test')
parser.add_argument('threads', type=int, help='number of parallel threads to start')
parser.add_argument('request_count', type=int, help='number of consecutive requests per thread')
args = parser.parse_args()

class BenchXMLRPC(threading.Thread):
    def __init__ (self, url, req_count):
        super(BenchXMLRPC, self).__init__()
        self.api = xmlrpc.client.ServerProxy(url)
        self._req_count = req_count
        self.__successful = 0
        self.__duration = 0

    def do_request(self):
        mhash = {'shared_secret': 'shai5Zio9ail7fieGaem2ooniVaig8ohteiDu9Ea' }
        testlines = ["test\n"]
        try:
            self.api.log_output(mhash, testlines)
        except Exception as e:
            return False
        return True

    def run(self):
        start = time.time()
        for _ in range(self._req_count):
            if self.do_request():
                self.__successful += 1
        end = time.time()
        self.__duration = end - start

    def print_status(self):
        print("{} successfull requests took: {} seconds".format(self.__successful, self.__duration))
        msecs_avg = (self.__duration * 1000) / self._req_count
        print("thats {} msecs per request.".format(msecs_avg))

threads = []
for i in range(args.threads):
    t = BenchXMLRPC(args.url, args.request_count)
    t.start()
    threads.append(t)

for t in threads:
    t.join()
    t.print_status()
