#!/bin/bash
# Creates venv if it dosen't exist and installs requirements.txt.
# Saves a checksum of requirements.txt, checks against that.
# Reruns pip install if checksum changes on subsequent runs.
# TODO:
# - Make python/ folder prefix customizable.
# - Use git version instead of crc32.
# Aaron Saderholm 7/16/17

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f "venv/bin/activate" ]; then
        virtualenv venv
else
        echo "virtualenv exists; skipping creation."
fi

source venv/bin/activate
REQPATH=$DIR/requirements.txt
REQCRC_FILE="$DIR/venv/req_checksum_$(crc32 "$REQPATH")"
# I'm assuming there's not going to be a file matching a checksum name in
# the root venv directory but you might not want to make that assumption.
if [ ! -f "$REQCRC_FILE" ]; then
        pip install -r $DIR/requirements.txt
        rm -f $DIR/venv/req_checksum*
        touch $REQCRC_FILE
else
        echo "found checksum matching requirements.txt, skipping install."
fi