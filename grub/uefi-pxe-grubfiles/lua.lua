#!lua

grub.setenv("prefix",grub.getenv("net_pxe_241") or "(pxe)/sccgrub2.c")
netstring=grub.getenv("net_pxe_240") or "global-default"
jpy=grub.getenv("net_pxe_242") or "(http,scc-pool-boot.scc.kit.edu:8099)/getcurrentconfig?mac="
mac = grub.getenv("mac") or grub.getenv("net_pxe_mac") or "ff:ff:ff:ff:ff:ff"
grub.setenv("mac",mac)
filex="configfile "..jpy..mac.."&netstring="..netstring
grub.setenv("filex",filex)

for i,x in pairs({"mac","net_pxe_240","net_pxe_241","net_pxe_242","net_pxe_243","net_pxe_244","prefix","filex"}) do
  if(grub.getenv(x)) then grub.run("export "..x) end
end


grub.run(filex)

