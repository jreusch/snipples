const WebSocket = require('ws');
const http = require('http');
const url = require('url');

const port = process.argv[2] || 8090

console.log("using port", port);

function registerHandlers(wss, text = undefined) {
    wss.on('connection', function connection(ws) {
        ws.on('message', function incoming(message) {
            console.log('received: %s', message);
        });

        const handle = setInterval(() => {
            console.log('sending');
            ws.send(text || 'something');
        }, 1000);

        ws.on('close', function close() {
            clearInterval(handle);
            console.log('socket closed');
        });
    });
}

function serverWithPath() {
    const server = http.createServer();
    const wss1 = new WebSocket.Server({noServer: true});
    const wss2 = new WebSocket.Server({noServer: true});

    registerHandlers(wss1, "foo something");
    registerHandlers(wss2, "bar something");

    server.on('upgrade', function upgrade(request, socket, head) {
        const pathname = url.parse(request.url).pathname;

        if (pathname === '/foo') {
            wss1.handleUpgrade(request, socket, head, function done(ws) {
                wss1.emit('connection', ws, request);
            });
        } else if (pathname === '/bar') {
            wss2.handleUpgrade(request, socket, head, function done(ws) {
                wss2.emit('connection', ws, request);
            });
        } else {
            socket.destroy();
        }
    });
    server.listen(port);
}

function simpleServer() {
    const wss = new WebSocket.Server({port});
    registerHandlers(wss);
}

function main() {
//    serverWithPath();
    simpleServer();
}


main();
