const WebSocket = require('ws');

const dest = process.argv[2] || "ws://127.0.0.1:8090";
console.log("connecting to", dest);
 
const ws = new WebSocket(dest);
 
ws.on('open', function open() {
    console.log("connection: open");
});

ws.on('error', function error(event) {
    console.log("connection: error", event);
});

ws.on('close', function close(event) {
    console.log("connection: close", event);
});
 
ws.on('message', function incoming(data) {
      console.log(data);
});
