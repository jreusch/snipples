const uglifyJS = require('uglify-js');
const fs = require('fs');
const glob = require('glob');

const globOption = {
    ignore: [
        'dist/client/jspm.config.js',
        'dist/client/node_modules/**/*.js',
        'dist/client/jspm_packages/**/*.js',
        'dist/client/assets/**/*.js',
    ],
};
const uglifyOptions = {
    keep_fnames: true,
};

glob('dist/@(shared|client)/**/*.js', globOption, (er, files) => {
    if (er) {
        throw er;
    }
    files.forEach((file) => {
        if (process.env.NODE_ENV === 'DEBUG') {
            console.log(`FILE => ${file}`);
        }
        try {
            const code = fs.readFileSync(file, 'utf8');
            const result = uglifyJS.minify(code, uglifyOptions);

            if (!result.error) {
                fs.writeFileSync(file, result.code, 'utf8', (error) => {
                    throw error;
                });
            } else {
                throw result.error;
            }
        } catch (error) {
            console.error('uglify script error: ', file);
            console.error('uglify script error: ', error);
            throw error;
        }
    });
});
