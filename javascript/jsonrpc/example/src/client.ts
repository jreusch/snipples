import * as rpc from "vscode-ws-jsonrpc";
// import * as WebSocket from 'ws';
const WebSocket = require("ws");

import {nullReplyRequest} from "./shared";

interface Test1 {
    value1: string;
    value2: string;
}

let interval;
const notification = new rpc.NotificationType<string, void>("testNotification");


function sendPeriodically(connection: rpc.MessageConnection) {
    interval = setInterval(() => {

        console.log("sending notification");
        connection.sendNotification(notification, "Hello World");

        const request = new rpc.RequestType<string, string, void, void>("jantest");
        console.log("sending request");
        connection.sendRequest(request, "test123").then((response) => {
            console.log("response", response);
        });

        const request2 = new rpc.RequestType<Test1, Test1, void, void>("jantest2");
        console.log("sending request2");
        connection.sendRequest(request2, {value1: "x1", value2: "x2"}).then((response) => {
            console.log("response 2", response);
        }).catch((error) => {
            console.error(error.code);
        });

        // server client/object mismatch behavior
        const request3 = new rpc.RequestType2<Test1, string, Test1, void, void>("jantest3");
        console.log("sending request3");
        connection.sendRequest(request3, {value1: "x1", value2: "x2"}, "second param").then((response) => {
            console.log("response 3", response);
        }).catch((error) => {
            console.error(error.code);
        });

    }, 5 * 1000);
}

function testNullReply(connection: rpc.MessageConnection) {
    interval = setInterval(async () => {


        console.log("sending request");
        connection.sendRequest(nullReplyRequest, "test promise").then((response) => {
            console.log("response promise", response);
        }).catch((error) => {
            console.error(error.code);
        });
        const result = await connection.sendRequest(nullReplyRequest, "test await");
        console.log("response await:", {result});

    }, 5 * 1000);
}

setTimeout(() => {

    console.log("starting");
    const webSocket = new WebSocket("ws://127.0.0.1:9999/test");
    webSocket.on("close", (event) => {
        console.error("ws close 2", event);
        clearInterval(interval);
    });
    // webSocket.on("error", (event) => {
    //     console.error("ws error", event);
    //     webSocket.close();
    // });

    rpc.listen({
        webSocket,
        onConnection: (connection: rpc.MessageConnection) => {
            console.log("connected");
            connection.listen();

            connection.onError(() => {
                console.log("rpc on error",);
            });
            connection.onClose(() => {
                console.log("rpc on close");
                // connection.dispose();
            });

            connection.onNotification(notification, (param: string) => {
                console.log("received: ", param); // This prints Hello World
            });

            //sendPeriodically(connection);
            testNullReply(connection);


        }
    });
}, 1000);
