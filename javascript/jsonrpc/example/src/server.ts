import * as rpc from 'vscode-ws-jsonrpc';
import * as WebSocket from 'ws';
import { HandlerResult } from 'vscode-ws-jsonrpc';

import {nullReplyRequest} from "./shared";

interface Test1 {
    value1: string;
    value2: string;
}

// just to see if we can handle additional attributes
interface Test2 {
    value1: string;
    value3: string;
}

const wss = new WebSocket.Server({ port: 9999, path: "/test"});
wss.on('connection', (ws) => {

//    console.log(ws.onMessage);
    //const socket: rpc.IWebSocket = ws;
    //const socket: rpc.IWebSocket = rpc.toSocket(wss);
    const socket: rpc.IWebSocket = rpc.toSocket(ws);
    const reader = new rpc.WebSocketMessageReader(socket);
    const writer = new rpc.WebSocketMessageWriter(socket);
    const logger = new rpc.ConsoleLogger();
    const connection = rpc.createMessageConnection(reader, writer);
    const notification = new rpc.NotificationType<string, void>('testNotification');
    connection.onNotification(notification, (param: string) => {
        console.log("received notification: ", param); // This prints Hello World
    });
    const request = new rpc.RequestType<string, string, void, void>("jantest");
    connection.onRequest(request, (params: string, ): string =>  {
        console.log("received request jantest, param:", params);
        connection.sendNotification(notification, 'in between notify');
        return "kam an!";
    })

    const request2 = new rpc.RequestType<Test1, Test1, void, void>("jantest2");
    connection.onRequest(request2, (params: Test1, ): Test1 =>  {
        console.log("received request jantest2, param:", params);
        connection.sendNotification(notification, 'in between notify');
        return {value1: "ret: " + params.value1 , value2: "ret: " + params.value2};
    })

    // server client/object mismatch behavior
    const request3 = new rpc.RequestType2<Test2, string, Test2, void, void>("jantest3");
    connection.onRequest(request3, (param1: Test2, param2: string ): Test2 =>  {
        console.log("received request jantest3, param:", {param1, param2});
        connection.sendNotification(notification, 'in between notify');
        return {value1: "ret: " + param1.value1 , value3: "ret: " + param1.value3 + " param2: " + param2};
    })

    connection.onRequest(nullReplyRequest, (param1: string): null => {
        console.log("received nullreplay request, param:", {param1});
        return null;
    });

    connection.listen();
});

