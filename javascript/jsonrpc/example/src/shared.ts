import * as rpc from "vscode-ws-jsonrpc";
export const nullReplyRequest = new rpc.RequestType<string, void, void, void>("nullreply");
