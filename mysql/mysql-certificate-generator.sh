#!/bin/sh

# See: http://bugs.mysql.com/bug.php?id=64870
# See: http://forums.mysql.com/read.php?11,400856,401127
openssl genrsa 1024 > mysql-cakey.pem
openssl req -new -x509 -nodes -days 10000 -key mysql-cakey.pem > mysql-cacert.pem
openssl req -newkey rsa:1024 -days 10000 -nodes -keyout mysql-serverkey.pem > mysql-serverreq.pem
openssl x509 -req -in mysql-serverreq.pem -days 10000 -CA mysql-cacert.pem -CAkey mysql-cakey.pem -set_serial 01 > mysql-servercert.pem
openssl req -newkey rsa:1024 -days 10000 -nodes -keyout mysql-clientkey.pem > mysql-clientreq.pem
openssl x509 -req -in mysql-clientreq.pem -days 1000 -CA mysql-cacert.pem -CAkey mysql-cakey.pem -set_serial 01 > mysql-clientcert.pem
~

