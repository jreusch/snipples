#!/bin/sh -e

URL="https://raw.githubusercontent.com/jnk22/kodinerds-iptv/master/iptv/kodi/kodi_tv_main.m3u"
BASE_PORT=8554

generic_label=1

usage() {
    echo -n "usage: `basename $0`"
    echo " <channel> <steamoffset>"
}

[ -z "$1" ] && usage && exit 1
stream=$1

[ -z "$2" ] && usage && exit 1
offset=$2

port=$((BASE_PORT+offset))
# does not work in dash?! -> bash works, tldp states this is standard conform?
#label=${stream// /_}

if [ -z "$generic_label" ]; then
  label=`echo $stream | tr ' ' '_'`
else
  label="stream${offset}"
fi

echo "Starting forwarding stream \"$stream\" on port $port"

play_url=`curl $URL | grep -A 1 -F "tvg-name=\"${stream}\"" | tail -n 1`

echo "playing url: $play_url"
echo "rtsp port $port"
echo "rtsp label $label"

exec cvlc --sout=#rtp{sdp=rtsp://:${port}/${label}} --sout-all --sout-keep --ttl 5 "$play_url"
