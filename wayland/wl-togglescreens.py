#!/usr/bin/python

# copied from https://git.project-insanity.org/onny/wl-togglescreens

import subprocess
import re

PREFERRED_MODES={'LG ULTRAFINE 210MAKRPFL45': '3840x2160'}

def get_preferred_mode(screenDescription):
    for key, value in PREFERRED_MODES.items():
        if screenDescription.find(key) != -1:
            return PREFERRED_MODES[key]


class Screen:

    def __init__(self, name, mode=None, state=None):
        self.name = name
        self.mode = mode
        self._state = state

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        if self._state != None:
            if value:
                cmd = ['wlr-randr', '--output', self.name, '--on']
                if self.mode is not None:
                    cmd += ['--mode', self.mode]
                print(f"toggle on: {cmd}")
                subprocess.run(cmd)
            else:
                subprocess.run(['wlr-randr', '--output', self.name, '--off'])
        self._state = value

class wlScreens:
    
    def __init__(self):
        self.screens = []
        result = subprocess.run(['wlr-randr'], stdout=subprocess.PIPE)
        result = result.stdout.decode('utf-8')
        re_search_pat = r'([a-zA-Z]+-\d) ("(.*)")?'
        match = re.findall(re_search_pat, result)
        for screenInfo in match:
            screen = Screen(screenInfo[0], get_preferred_mode(screenInfo[2]))
            self.screens.append(screen)
        re_search_pat = r'Enabled: ([a-z]+)'
        match = re.findall(re_search_pat, result)
        for num, state in enumerate(match):
            screen = self.screens[num]
            if (state == "yes"):
                screen.state = True
            else:
                screen.state = False

    def toggleModes(self):
        if (len(self.screens) == 1):
            if (self.screens[0].state == True):
                self.screens[0].state = False
            else:
                self.screens[0].state = True
        elif (len(self.screens) == 2):
            if (self.screens[0].state == True and self.screens[1].state == True):
                self.screens[0].state = False
            elif (self.screens[0].state == False and self.screens[1].state == True):
                self.screens[1].state = False
                self.screens[0].state = True
            elif (self.screens[0].state == True and self.screens[1].state == False):
                self.screens[1].state = True

    def __len__(self):
        return len(self.screens)

    def __str__(self):
        return str(self.screens)

    def __getitem__(self, key):
        return self.screens[key]

screens = wlScreens()
screens.toggleModes()
